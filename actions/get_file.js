const MongoObjectId = require('mongodb').ObjectId
const distribution_config = require('../../distribution_config/conifg.json')
var cassandra_client = null

class GetFile {//costs $ 0.29/ million request /month

    static async select_file_as_owner( _owner_id, _file_id){
        //console.log(_file_id.toString().substr(0, 6), _file_id.toString().substr(6, 24));
        const file_id  = _file_id.toString().substr(0, 6)
                        + MongoObjectId(_file_id.toString().substr(6, 24)).toString()
        const owner_id   = `${MongoObjectId(_owner_id)}`
        const query = `SELECT * FROM file_store`
        + ` WHERE id = '${file_id}' AND owner_id = '${owner_id}' ALLOW FILTERING;`
        const result = await cassandra_client.execute(query)
        return result.rows.length?result.rows[0]:null;
    }
    static async select_file_as_public(_file_id){ //prefer this, very fast
        const file_id  = _file_id.toString().substr(0, 6)
        + MongoObjectId(_file_id.toString().substr(6, 24)).toString()
        const query = `SELECT * FROM file_store`
        + ` WHERE id = '${file_id}';`
        const result = await cassandra_client.execute(query)
        return result.rows.length?result.rows[0]:null;
    }
}

module.exports = ( (_cassandra_client)=>{
    cassandra_client = _cassandra_client
    return GetFile
})